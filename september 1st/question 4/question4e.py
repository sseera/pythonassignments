#taking the input
n=int(input("Enter the number of key-value pairs you want to print : " ))
#take the empty dictionary to store the resultant value
d = dict()

for x in range(1,n+1):
    d[x]=x*x

#printing the resultant dictionary
print(d) 

print(sum(d.values()))
print(sum(d.keys()))
