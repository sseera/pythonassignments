#Python program to sort (ascending and descending) a dictionary by key

import operator

#input dictionary
ex_dict = {1: 2, 3: 4, 4: 3, 2: 1, 0: 0}

#use the sorted method to sort the dictionary by key
asc = dict(sorted(ex_dict.items(), key=operator.itemgetter(0)))
print('Ascending order by value : ',asc)

#for the descending order by keeping the value of reverse to True  
desc = dict( sorted(ex_dict.items(), key=operator.itemgetter(0),reverse=True))
print('Descending order by value : ',desc)
