#Python code to merge dictionary
def Merge(dict1, dict2):
	for i in dict2.keys():
		dict1[i]=dict2[i]
	return dict1

#first dictionary	
dict1 = {'a': 1, 'b': 2}
#second dictionary
dict2 = {'c': 3, 'd': 4}

#calling the merge function by taking the two dictionaries as arguments
#and store it in the new variable
dict3 = Merge(dict1, dict2)

#print the resultant dictionary
print(dict3)
