# Taking the file_name as input
file_name = input()
extention = input()

#Reversing the filename 
file_name_new = file_name[::-1]

f1 = open(file_name_new+extention, "w")

# Open the input file and get the content into a variable data
with open(file_name+extention, "r") as myfile:
	data = myfile.read()

# Data will be stored in resultant_data in reverse
resultant_data = data[::-1]

#Writing the reversed data into the newfile
f1.write(resultant_data)

f1.close()
