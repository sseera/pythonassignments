import pandas as pd

filepath=""


def add_total_percent_columns(filepath):

  df = pd.read_csv(filepath)

  df["Total"] = df[['math', 'science','arts']].sum(axis=1) 
  df["Percentage"]= df[["Total"]].div(300)
  print(df)

  df.to_csv(filepath,index_col = False)


add_total_percent_columns(filepath)
