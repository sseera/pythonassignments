'''
Python program to Read the file using numpy and
Finding Max Values of each rows
'''

import numpy as np

a = np.genfromtxt("test.csv", delimiter=",")

max_value = np.amax(a, axis=1)

print(max_value)

