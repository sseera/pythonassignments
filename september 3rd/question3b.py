'''
	Python program to Read the file using numpy and
  Sorting the data by 3rd column
'''

import numpy as np

data = np.genfromtxt("test.csv", delimiter=",")


def sort_array(x_new, column=None, flip=False):
    '''
    function to sort the numpy array
    ...
    Parameters
    ----------
    x : list of values
    Returns
    -------
    list (elements of the list)
    '''
    x_new = x_new[np.argsort(x_new[:, column])]
    if flip:
        x_new = np.flip(x_new, axis=0)
    return x_new


# function call to sort the array
data = sort_array(data, column=3, flip=False)
print(data)

