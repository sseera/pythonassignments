'''
	Python program to Read the file using numpy and
	Finding Max Values of each column
'''

import numpy as np

a = np.genfromtxt('test.csv', delimiter=',',skip_header=1)

max_value = np.amax(a, axis = 0)

print(max_value)

